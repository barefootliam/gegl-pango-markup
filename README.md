The gegl-markup GEGL plug-in
=========


Use this to get rich text in GIMP and GEGL, complete with OpenType feature support.

You can run it from the GIMP GEGL Graph plug-in like this:

```
pango-markup text="<span font_size='40000' line-height='1.2' fgcolor='#998833' font='IM Fell English Pro' font-features='liga on, clig on, dlig on, hist on'>Perfect shadows in a sunshine day?
Difficult steps to issue forth</span>"
```

This gives:

![image preview](images/marlowe.png)

You can read more about the syntax at the Pango page:

https://docs.gtk.org/Pango/pango_markup.html

Note that you should not include the outer `<markup>` and `</markup>` tags described on that page.

This is a _source_ operation: it creates image data, replacing whatever was in the layer before it. It's best to create a new layer for it to replace; the layer will be resized to fit the text.

The properties it takes are:
* _text_ - the XML markup to render;
* _color_ - probably ignored, use markup to set a colour, but defaults to black;
* _wrap_ - width in pixels: lines longer than this will be wrapped and formatted as a paragraph;
* _alignment_ - Justification: 0 for left, 1 for center, 2 for right. This only makes sense if you give _wrap_ as well.
* _vertical_alignment_ - 0 top, 1 middle, 2 bottom; may or may not work for you.
* _default line spacing_ - the distance between baselines (multiple of font size). Use this if setting line-height in CSS doesn't work, as line-height is only available in newer pango releases.

More properties may be added in teh future, in particular for writing mode/direction.

# Installation

There is a Linux binary, but ideally you should recompile it.

## Where to Put GEGL Filter binaries 

### Windows

```
C:\\Users\alfonso\AppData\Local\gegl-0.4\plug-ins
```
Change `alfonso` to your Windows username.
 
### Linux 

```
/home/alfonso/.local/share/gegl-0.4/plug-ins/
```
Again, change alfonso to your username, or use $HOME or `~` instead of `/home/alfonso`

If you installed GIMP via a flatpak, you will need to find the `gegl-0.4` folder (or 0.5 maybe, in the future). A good place to look is:

```
$HOME/.var/app/org.gimp.GIMP/data/gegl-0.4/plug-ins
```


## Compiling and Installing

### Linux and Unix

To compile and install you will need the GEGL header files (`libgegl-dev` on
Debian based distributions or `gegl` on Arch Linux) and meson (`meson` on
most distributions). You will also need `pango`, `cairo` and `pangocairo` and their respective development packages.

```bash
SRC_DIR=$(pwd)
BUILD_DIR=${SRC_DIR}/obj-$(arch)
mkdir -p $BUILD_DIR && cd $BUILD_DIR && meson -Dprefix=$PREFIX --buildtype=release $SRC_DIR && ninja && ninja install
```
This will create a .o file in the obj-x86_64 folder (it might have a different name depending on your CPU type and operating system).

If you have an older version of gegl you may need to copy to `~/.local/share/gegl-0.3/plug-ins`
instead



### Windows

The easiest way to compile this project on Windows is by using msys2.  Download
and install it from here: https://www.msys2.org/

Open a msys2 terminal with `C:\msys64\mingw64.exe`.  Run the following to
install required build dependencies:

```bash
pacman --noconfirm -S base-devel mingw-w64-x86_64-toolchain mingw-w64-x86_64-meson mingw-w64-x86_64-gegl
```

Then build the same way you would on Linux:

```bash
SRC_DIR=$(pwd)
BUILD_DIR=${SRC_DIR}/obj-$(arch)
mkdir -p $BUILD_DIR && cd $BUILD_DIR && meson -Dprefix=$PREFIX --buildtype=release $SRC_DIR && ninja
```

Then copy the DLL file into this folder:
```
C:\\Users\<YOUR NAME>\AppData\Local\gegl-0.4\plug-ins
```

### Macos

I do not have access to a Mac; you will likely need to follow the Linux instructions as best you can, and then mark the resulting dynamic library (the .so file) as not needing signing,

```bash
sudo xattr ~/Library/Application\ Support/gegl/0.4/plug-ins/pango-markup.so
```

## Acknowledgements

See more GEGL plug-ins at https://github.com/LinuxBeaver?tab=repositories
which is also where i got the idea of writing this as a plug-in, and initial files for examples.

