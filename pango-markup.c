/* This file is an image processing operation for GEGL
 *
 * GEGL is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <https://www.gnu.org/licenses/>.
 *
 * Copyright 2006 Øyvind Kolås <pippin@gimp.org>
 *           2022 Liam Quin (barefootliam) pango-markup
 */

#include "config.h"
#include <glib/gi18n-lib.h>

#ifdef GEGL_PROPERTIES

property_string (text, _("Markup"), "Hello")
    description(_("Pango XML markup fragment to display (utf8, no outer markup element)"))
    ui_meta    ("multiline", "true")

property_color  (color, _("Color"), "black")
    /* TRANSLATORS: the string 'black' should not be translated */
    description(_("Color for the text (defaults to 'black')"))

property_int  (wrap, _("Wrap width"), -1)
    description (_("Sets the width in pixels at which long lines will wrap. "
                     "Use -1 for no wrapping."))
    ui_meta ("unit", "pixel-distance")
    value_range (-1, 1000000)

property_int  (vertical_wrap, _("Wrap height"), -1)
    description (_("Sets the height in pixels according to which the text is "
                   "vertically justified. "
                   "Use -1 for no vertical justification."))
    ui_meta ("unit", "pixel-distance")
    value_range (-1, 1000000)

property_int    (alignment, _("Justification"), 0)
    value_range (0, 2)
    description (_("Alignment for multi-line text (0=Left, 1=Center, 2=Right)"))

property_int    (vertical_alignment, _("Vertical justification"), 0)
    value_range (0, 2)
    description (_("Vertical text alignment (0=Top, 1=Middle, 2=Bottom)"))

property_double (line_spacing, _("Default line spacing"), 1.15)
    value_range (-100, 1000000)
#else

#include <gegl-plugin.h>
#include <cairo.h>
#include <pango/pango-attributes.h>
#include <pango/pangocairo.h>



struct _GeglOp
{
  GeglOperationSource parent_instance;
  gpointer            properties;
  void *user_data;
};

typedef struct
{
  GeglOperationSourceClass parent_class;
} GeglOpClass;


#define GEGL_OP_NAME     pango_markup
#define GEGL_OP_C_SOURCE pango-markup.c



#include "gegl-op.h"
GEGL_DEFINE_DYNAMIC_OPERATION (GEGL_TYPE_OPERATION_SOURCE)

typedef struct {
  gchar         *text;
  gint           wrap;
  gint           vertical_wrap;
  gint           alignment;
  gint           vertical_alignment;
  gdouble	 line_spacing;
  GeglRectangle  defined;

  GeglOperationSource parent_instance;
  gpointer            properties;
} PM_UserData;

static void
markup_layout_text (GeglOp        *self,
		    cairo_t       *cr,
		    GeglRectangle *bounds,
		    int            component_set)
{
  GeglProperties       *o = GEGL_PROPERTIES (self);
  PangoLayout          *layout;
  PangoAttrList        *attrs;
  guint16               color[4];
  gchar                *text;
  gint                  alignment = 0;
  PangoRectangle        ink_rect;
  PangoRectangle        logical_rect;
  gint                  vertical_offset = 0;


  layout = pango_cairo_create_layout (cr);
  PM_UserData *userData = o->user_data;
  if (!userData) {
    o->user_data = userData = g_malloc0 (sizeof (PM_UserData));
    userData->text = 0;
  }
  if (!o->text) {
    return;
  }

  text = g_strcompress (o->text);
  pango_layout_set_line_spacing (layout, o->line_spacing);
  pango_layout_set_markup_with_accel (layout, text, -1, 0, NULL);
  g_free (text);

  switch (o->alignment)
  {
  case 0:
    alignment = PANGO_ALIGN_LEFT;
    break;
  case 1:
    alignment = PANGO_ALIGN_CENTER;
    break;
  case 2:
    alignment = PANGO_ALIGN_RIGHT;
    break;
  }
  pango_layout_set_alignment (layout, alignment);
  pango_layout_set_width (layout, o->wrap * PANGO_SCALE);

  attrs = pango_attr_list_new ();

  switch (component_set)
  {
    case 0:
      gegl_color_get_pixel (o->color, babl_format ("R'G'B'A u16"), color);
      break;
    case 1:
      gegl_color_get_pixel (o->color, babl_format ("cykA u16"), color);
      break;
    case 2:
      gegl_color_get_pixel (o->color, babl_format ("cmkA u16"), color);
      break;
  }


  pango_attr_list_insert (
    attrs,
    pango_attr_foreground_new (color[0], color[1], color[2]));
  pango_attr_list_insert (
    attrs,
    pango_attr_foreground_alpha_new (color[3]));

#if 0
  pango_layout_set_attributes (layout, attrs);

  /* Inform Pango to re-layout the text with the new transformation */
  pango_cairo_update_layout (cr, layout);
#endif

  pango_layout_get_pixel_extents (layout, &ink_rect, &logical_rect);
  if (o->vertical_wrap >= 0)
    {
      switch (o->vertical_alignment)
      {
      case 0: /* top */
        vertical_offset = 0;
        break;
      case 1: /* middle */
        vertical_offset = (o->vertical_wrap - logical_rect.height) / 2;
        break;
      case 2: /* bottom */
        vertical_offset = o->vertical_wrap - logical_rect.height;
        break;
      }
    }

  if (bounds)
    {
      *bounds = *GEGL_RECTANGLE (ink_rect.x,     ink_rect.y + vertical_offset,
                                 ink_rect.width, ink_rect.height);
    }
  else
    {
      /* When alpha is 0, Pango goes full alpha (by design).  Go figure... */
      if (color[3] > 0)
        {
          cairo_translate (cr, 0, vertical_offset);

          pango_cairo_show_layout (cr, layout);
        }
    }

  pango_attr_list_unref (attrs);
  g_object_unref (layout);
}

static gboolean
process (GeglOperation       *operation,
         GeglBuffer          *output,
         const GeglRectangle *result,
         gint                 level)
{
  GeglOp *self = GEGL_OP (operation);
  const Babl *format =  gegl_operation_get_format (operation, "output");
  const Babl *formats[4] = {NULL, NULL, NULL, NULL};
  int is_cmyk = babl_get_model_flags (format) & BABL_MODEL_FLAG_CMYK ? 1 : 0;

  cairo_t         *cr;
  cairo_surface_t *surface;
  if (is_cmyk)
  {
    formats[0]=babl_format ("cairo-ACYK32");
    formats[1]=babl_format ("cairo-ACMK32");
  }
  else
  {
    formats[0]=babl_format ("cairo-ARGB32");
  }

  for (int i = 0; formats[i]; i++)
  {
    guchar *data;
    data  = g_new0 (guchar, result->width * result->height * 4);

    surface = cairo_image_surface_create_for_data (data,
                                                 CAIRO_FORMAT_ARGB32,
                                                 result->width,
                                                 result->height,
                                                 result->width * 4);
    cr = cairo_create (surface);
    cairo_translate (cr, -result->x, -result->y);
#if 0
    cairo_set_source_rgba(cr, 0, 0, 0, 0.5);
    cairo_fill(cr);
#endif
    markup_layout_text (self, cr, NULL, i+is_cmyk);

    gegl_buffer_set (output, result, 0, formats[i], data,
                     GEGL_AUTO_ROWSTRIDE);

    cairo_destroy (cr);
    cairo_surface_destroy (surface);
    g_free (data);
  }

  return  TRUE;
}

static GeglRectangle
get_bounding_box (GeglOperation *operation)
{
  GeglOp *self = GEGL_OP (operation);
  GeglProperties           *o = GEGL_PROPERTIES (self);
  PM_UserData *userData = o->user_data;
  gint status = FALSE;

  if (!userData) {
    o->user_data = userData = g_malloc0 (sizeof (PM_UserData));
    userData->text = 0;
  }

  if ((userData->text && strcmp (userData->text, o->text)) ||
      !userData->defined.width ||
      userData->wrap != o->wrap ||
      userData->vertical_wrap != o->vertical_wrap ||
      userData->alignment != o->alignment ||
      userData->vertical_alignment != o->vertical_alignment ||
      userData->line_spacing != o->line_spacing)
      { /* get extents */
      cairo_t *cr;
      cairo_surface_t *surface  = cairo_image_surface_create (CAIRO_FORMAT_ARGB32,
          1, 1);
      cr = cairo_create (surface);
      markup_layout_text (self, cr, &userData->defined, 0);
      cairo_destroy (cr);
      cairo_surface_destroy (surface);

      if (userData->text)
	{
	  g_free (userData->text);
	}
      userData->text = g_strdup (o->text);
      userData->wrap = o->wrap;
      userData->vertical_wrap = o->vertical_wrap;
      userData->vertical_alignment = o->vertical_alignment;
      userData->line_spacing = o->line_spacing;
    }

  if (status)
    {
      g_warning ("get defined region for text '%s' failed", o->text);
    }

  return userData->defined;
}

static void
finalize (GObject *object)
{
  GeglOp *self = GEGL_OP (object);
  GeglProperties           *o = GEGL_PROPERTIES (self);
  PM_UserData *userData = o->user_data;

  if (userData)
    {
      if (userData->text)
      {
	g_free (userData->text);
      }
      g_free (userData);
      o->user_data = NULL;
    }
  G_OBJECT_CLASS (gegl_op_parent_class)->finalize (object);
}

static void
prepare (GeglOperation *operation)
{
  GeglProperties *o = GEGL_PROPERTIES (operation);
  const Babl *color_format = gegl_color_get_format (o->color);
  BablModelFlag model_flags = babl_get_model_flags (color_format);
  PM_UserData *userData = o->user_data;

  if (model_flags & BABL_MODEL_FLAG_CMYK)
  {
    gegl_operation_set_format (operation, "output",
                               babl_format ("camayakaA u8"));
  }
  else
  {
    gegl_operation_set_format (operation, "output",
                               babl_format ("RaGaBaA float"));
  }
  if (!userData) {
    o->user_data = userData = g_malloc0 (sizeof (PM_UserData));
    userData->text = 0;
  }
}

static const gchar *composition =
    "<?xml version='1.0' encoding='UTF-8'?>"
    "<gegl>"
    "<node operation='gegl:crop' width='200' height='200'/>"
    "<node operation='gegl:markup'>"
    "  <params>"
    "    <param name='wrap'>200</param>"
    "    <param name='color'>green</param>"
    "    <param name='text'>loves or pursues or desires to <i>obtain</i> pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious <b>physical exercise</b>, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no</param>"
    "  </params>"
    "</node>"
    "</gegl>";

static void
gegl_op_class_init (GeglOpClass *klass)
{
  GObjectClass             *object_class;
  GeglOperationClass       *operation_class;
  GeglOperationSourceClass *operation_source_class;

  object_class    = G_OBJECT_CLASS (klass);
  operation_class = GEGL_OPERATION_CLASS (klass);
  operation_source_class = GEGL_OPERATION_SOURCE_CLASS (klass);

  object_class->finalize = finalize;
  operation_class->prepare = prepare;
  operation_class->get_bounding_box = get_bounding_box;
  operation_source_class->process = process;

  gegl_operation_class_set_keys (operation_class,
    "reference-composition", composition,
    "title",          _("Render Pango Markup"),
    "name",           "gegl:pango-markup",
    "categories",     "render",
    "reference-hash", "deafbededeafbededeafbededeafbede",
    "description",  _("Display a string containing XML-style marked-up text using Pango and Cairo."),
    NULL);

}


#endif
